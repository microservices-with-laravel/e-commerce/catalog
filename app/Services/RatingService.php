<?php

namespace App\Services;

use App\Containers\RatingContainer;
use Illuminate\Support\Collection;

class RatingService
{
    public function __construct(private HttpClient $httpClient)
    {
    }

    /**
     * @param Collection<Product> $products
     * @return Collection<RatingContainer>
     */
    public function getRatings(Collection $products): Collection
    {
        return $this->httpClient
            ->get('products/ratings', [
                'productIds' => $products->pluck('id')->toArray()
            ])
            ->map(fn (array $rating) => new RatingContainer(...$rating));
    }
}
