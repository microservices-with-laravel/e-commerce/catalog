<?php

namespace App\Services;

use App\DataTransferObjects\CatalogSearchData;
use Ecommerce\Common\Containers\Product\ProductContainer;
use Illuminate\Support\Collection;

class ProductService
{
    public function __construct(private HttpClient $httpClient)
    {
    }

    /**
     * @return Collection<ProductContainer>
     */
    public function getProducts(CatalogSearchData $data): Collection
    {
        return $this->httpClient
            ->get('products', $data->toArray())
            ->map(fn (array $product) => ProductContainer::fromArray($product));
    }
}
