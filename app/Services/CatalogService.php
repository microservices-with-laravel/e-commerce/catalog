<?php

namespace App\Services;

use App\Containers\CatalogContainer;
use App\Containers\RatingContainer;
use App\DataTransferObjects\CatalogSearchData;
use Ecommerce\Common\Containers\Product\ProductContainer;
use Ecommerce\Common\Containers\Warehouse\InventoryContainer;
use Illuminate\Support\Collection;

class CatalogService
{
    public function __construct(
        private ProductService $productService,
        private WarehouseService $warehouseService,
        private RatingService $ratingService
    ) {
    }

    /**
     * @return Collection<CatalogContainer>
     */
    public function getCatalog(CatalogSearchData $data): Collection
    {
        $products = $this->productService->getProducts($data);
        if ($products->isEmpty()) {
            return collect([]);
        }

        $inventories = $this->warehouseService->getAvailableInventories($products);
        $ratings = $this->ratingService->getRatings($products);
        $catalog = [];

        foreach ($products as $product) {
            $inventory = $inventories->firstWhere('productId', $product->id);
            $rating = $ratings->firstWhere('productId', $product->id);

            if ($inventory->quantity === 0.0) {
                continue;
            }

            $catalog[] = $this->createContainer($product, $rating, $inventory);
        }

        return collect($catalog);
    }

    private function createContainer(
        ProductContainer $product,
        RatingContainer $rating,
        InventoryContainer $inventory
    ): CatalogContainer {
        return new CatalogContainer(
            $product->id,
            $product->name,
            $product->description,
            $product->price,
            $rating->averageRating,
            $rating->numberOfRatings,
            $inventory->quantity
        );
    }
}
