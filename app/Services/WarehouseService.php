<?php

namespace App\Services;

use Ecommerce\Common\Containers\Product\ProductContainer;
use Ecommerce\Common\Containers\Warehouse\InventoryContainer;
use Illuminate\Support\Collection;

class WarehouseService
{
    public function __construct(private HttpClient $httpClient)
    {
    }

    /**
     * @param Collection<ProductContainer> $products
     * @return Collection<InventoryContainer>
     */
    public function getAvailableInventories(Collection $products): Collection
    {
        return $this->httpClient
            ->get('inventory/products', [
                'productIds' => $products->pluck('id')->toArray()
            ])
            ->map(fn (array $inventory) => new InventoryContainer(...$inventory));
    }
}
