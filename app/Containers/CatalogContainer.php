<?php

namespace App\Containers;

class CatalogContainer
{
    public int $numberOfRatings;

    public function __construct(
        public int $id,
        public string $name,
        public string $description,
        public float $price,
        public ?float $averageRating,
        ?int $numberOfRatings,
        public float $availableQuantity
    ) {
        $this->numberOfRatings = (int) $numberOfRatings;
    }
}
