<?php

namespace App\Containers;

class RatingContainer
{
    public function __construct(
        public int $productId,
        public ?float $averageRating,
        public int $numberOfRatings
    ) {

    }
}
